#!/bin/sh

set -eu
#exec 1>&2

ret=0
SAVEGAMEDIR=~/.local/share/ironseed
TESTS_DIR=debian/tests

test -d "${AUTOPKGTEST_ARTIFACTS}"
test -d "${SAVEGAMEDIR}" && rm -rf "${SAVEGAMEDIR}"
mkdir -p "${SAVEGAMEDIR}"

# run game in virtual X11 server, with safety timeout
X_LOG="${AUTOPKGTEST_ARTIFACTS}/x_savegames2.log"

# this sed is workaround for https://gitlab.freedesktop.org/mesa/mesa/-/issues/10293
timeout -k100 -v 300 xvfb-run --auto-servernum --error-file="${X_LOG}" --server-args='-screen 0 640x480x24' ${TESTS_DIR}/savegames_x \
  2>&1 | sed -e '/MESA: error: ZINK: vkCreateInstance failed\|failed to load driver: zink\|glx: failed to create drisw screen/d' 1>&2

# ignore unimportant warnings
sed -i.bak -e '/The XKEYBOARD keymap compiler\|Could not resolve keysym XF86\|Errors from xkbcomp are not fatal\|_XSERVTransmkdir:/d' ${X_LOG}

if [ -s "${X_LOG}" ]
then
	echo >&2 "X Logfile ${X_LOG} is not empty"
	ret=11
fi

if [ -d "${SAVEGAMEDIR}" ]; then
  cp -a "${SAVEGAMEDIR}" $AUTOPKGTEST_ARTIFACTS/savegames.ironseed
fi

for s in ${TESTS_DIR}/save?.sha256
do
  sed "s,~,${HOME},g" < "${s}" > "${s}.tmp"
  sha256sum -c "${s}.tmp"
  rm -f "${s}.tmp"
done

# verify if pictures are correctly oriented with correct colors
for s in ${TESTS_DIR}/artifact?.sha256
do
  sed "s,\./,${AUTOPKGTEST_ARTIFACTS}/,g" < "${s}" > "${s}.tmp"
  sha256sum -c "${s}.tmp"
  rm -f "${s}.tmp"
done

exit $ret
